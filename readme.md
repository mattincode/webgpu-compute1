# WebGPU - Compute example 1

Personal development learning the basics of WebGPU.

## Project setup

1. Get project: **git clone https://gitlab.com/mattincode/webgpu-compute1.git**
2. Enter _webgpu-compute1_-folder and run **npm install**
3. Run using: **npm start**

### Goals:

- Project setup including HMR, Typescript with experimental WebGPU-types
- Install Chrome Canary and get webGPU up and running
- Setup Chrome Canary VS Code debugging /Fail for now (use debugging inside Chrome instead for now)!
- WebGPU feature detection
- Get GPUDevice and display information
- Example1: Write to a buffer from CPU and hand over to GPU
- Example1: GPU buffer copying
- Setup compute shader compilation (@webgpu/glslang)
- Setup compute pipeline
- Perform computation and read the result
- Perform larger computations
- Investigate use cases for future web applications where computation can be done on device for faster response, more roboust applications etc..
  - ML/DNN workloads on device:
    x. Current version of Tensorflow.js uses WebGL for limited GPU-acceleration (no compute shaders available), and CPU-based calculations in WebAssembly.
    y. In the future, Tensorflow.js and similar will use WebGPU for near native performance (there is an experimental version: https://github.com/tensorflow/tfjs/tree/master/tfjs-backend-webgpu).
  - Specific customer case "Guard staff planning"
    x. The problem of creating the proper datastructures and algorithms that benefit from this is quite hard.
    y. For a typical GPU use-case to be beneficial a matrix multiplication for example starts to yield order of magnitude improvements over the CPU when exceeding 512x512 matrices (minor benefits yearlier).
    z. Above (y) is the pure matrix-multiplation scenario, not when comparing to the current type of branching logic with unoptimal datastructures in existing implementations.
  - Complex web-applications:
    x. The emergence of WebAssmembly togheter with WebGPU creates an unique opportunity to build true cross-platform roboust and performant applications.
    y. As usual a new ecosystem of frameworks will emerge but for bigger (and more long term) applications it is often better to not use too many abstractions.
- Show simple triangle using webGPU graphics pipeline

## Basis for this personal development project

- Topic: WebGPU

  - https://en.wikipedia.org/wiki/WebGPU
  - Reasoning: Will become the standard for web computing(AI) and graphics.

- Main specification and source:
  - https://gpuweb.github.io/gpuweb/
  - https://www.w3.org/community/gpu/
  - https://github.com/gpuweb/gpuweb
  - All repos including TS_types: https://github.com/gpuweb
- Industry support:
  - https://www.w3.org/community/gpu/participants
    - Community group include: Apple, Google, Microsoft, Huawei, Intel, Alibaba, Mozilla, LG, Samsung,....
  - https://github.com/gpuweb/gpuweb/wiki/Implementation-Status
- Resources:
  - Links:
    - https://github.com/gpuweb/gpuweb/wiki/Implementation-Status
    - https://developers.google.com/web/updates/2019/08/get-started-with-gpu-compute-on-the-web
    - https://austineng.github.io/webgpu-samples/?wgsl=0#helloTriangle
    - https://devsday.ru/blog/details/16754
    - https://hacks.mozilla.org/2020/04/experimental-webgpu-in-firefox/
    - https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Experimental_features
  - Example source code:
    - https://github.com/BabylonJS/Babylon.js/tree/WebGPU
    - https://github.com/austinEng/webgpu-samples
  - Demos:
    - https://austineng.github.io/webgpu-samples/?wgsl=0#helloTriangle
    - https://www.babylonjs.com/demos/webgpu/onespherewebgpu
    - https://www.babylonjs.com/demos/webgpu/forestwebgpu
- Development environment:
  - Chrome Canary: https://www.google.com/chrome/canary/
    - chrome://flags/#enable-unsafe-webgpu
    - Verify, browse to: https://austineng.github.io/webgpu-samples/?wgsl=1#helloTriangle
  - There is support in Edge Canary, Safary Tech Preview (from release 91) and Firefox nightly as well.
  - Use VS Code and my own basic React template to enable fast development.
- Research:
  - Read up on progress:
    - https://github.com/gpuweb/gpuweb/wiki
    - https://github.com/gpuweb/gpuweb/wiki/Implementation-Status
  - Check issues:
    - WGSL: https://github.com/gpuweb/gpuweb/issues/566
  - Early adopters:
    - Babylon.js: https://doc.babylonjs.com/extensions/webgpu
  - Some key points picked from MoM etc. from the meetings:
    - In line with my own thoughts, develop in native, deploy to web: "wgpu-native demo very-very quickly - not running in a browser, but using the native application targeting webGPU. Having a native implementation allows native development easily portable to web via wasm."
  - Notes:
    - Both AI(ML/DNN) and Graphics share the parallellt nature of massive matrix-operations that can be performed in parallell.
    - GPU:s are and will be the preferred way to execute theese workloads both in datacenters and outside in the forseeable future.
    - Vulkan/Metal/DirectX and WebGPU have a very similar api surface.
    - All above are low level api:s that deliver great performance by beeing more explicit towards the GPU.
    - There are a lot of parties involved (phone vendors, OS, datacenter..), a standard that span all type of devices from phone to datacenter will take time.
    - There is a lot of discussion around the shader language to use on the web, There is suggestions for WGSL or a DirectX-derivate(x-HLSL) but nothing is settled.
    - On a higher level, there is already a lot of compilation support from DirectX to SPIR-V, WebGL to SPIR-V. Why couldn't the WebAsm-approach be used.. no one writes WebAsm.. you use Rust,C/C++ etc. it's just a compile target.. same for SPIR-V.. let the tools (LLVM, Emscripten..) handle the shader-conversion from a native language to SPIR-V.
    - Use https://www.npmjs.com/package/@webgpu/glslang to compile compute shaders to SPIR-V that Chrome Canary can use.
    - The API is constantly changing and currently unsafe. As GPU sandboxing isn't implemented yet for the WebGPU API, it is possible to read GPU data for other processes! Don’t browse the web with it enabled
    - Since I've done some minor work with Vulkan before the API feels quite familiar (settings up buffers and commands).
