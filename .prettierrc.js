module.exports = {
  semi: true,
  trailingComma: "all",
  jsxBracketSameLine: true,
  arrowParens: "avoid",
  singleQuote: true,
  printWidth: 150,
  tabWidth: 2,
  endOfLine: "auto",
};