import ReactDOM from 'react-dom';
import React from 'react';
import App from './App';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import { hasWebGPUSupport } from './featureDetection';

if (!hasWebGPUSupport()) {
  alert(`WebGPU not supported in this browser, please see: https://github.com/gpuweb/gpuweb/wiki/Implementation-Status for details!`);
} else {
  ReactDOM.render(<App name="test" />, document.getElementById('root'));
}
