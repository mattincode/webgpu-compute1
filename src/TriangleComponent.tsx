//Note: Lazily using a React component here which is not the best use case when using a Canvas, this is just prototyping code.. do not copy!
import React from 'react';
import './App.css';
import glslangModule from './glslang';
import { getDevice } from './webGpuHelpers';

export interface ITriangleComponentProps {}

interface ITriangleComponentState {}

class TriangleComponent extends React.Component<ITriangleComponentProps, ITriangleComponentState> {
  private canvasRef: HTMLCanvasElement;
  private gpu: GPUDevice = null;
  private swapChain: GPUSwapChain = null;
  private pipeline: GPURenderPipeline = null;
  constructor(props: ITriangleComponentProps) {
    super(props);
    this.update = this.update.bind(this);
  }

  componentDidMount() {
    getDevice().then(gpu => {
      this.gpu = gpu;
      glslangModule().then(glslCompiler => {
        const swapChainFormat = 'bgra8unorm';
        this.pipeline = gpu.createRenderPipeline({
          vertexStage: {
            module: gpu.createShaderModule({
              code: glslCompiler.compileGLSL(glslShaders.vertex, 'vertex'),
            }),
            entryPoint: 'main',
          },
          fragmentStage: {
            module: gpu.createShaderModule({
              code: glslCompiler.compileGLSL(glslShaders.fragment, 'fragment'),
            }),
            entryPoint: 'main',
          },
          primitiveTopology: 'triangle-list',
          colorStates: [
            {
              format: swapChainFormat,
            },
          ],
        });

        const context = (this.canvasRef.getContext('gpupresent') as any) as GPUCanvasContext;
        this.swapChain = context.configureSwapChain({ device: this.gpu, format: swapChainFormat });
        requestAnimationFrame(this.update);
      });
    });
  }

  update(time: number) {
    const commandEncoder = this.gpu.createCommandEncoder();
    const textureView = this.swapChain.getCurrentTexture().createView();
    const renderPassDescriptor: GPURenderPassDescriptor = {
      colorAttachments: [
        {
          attachment: textureView,
          loadValue: { r: 0.0, g: 0.0, b: 0.0, a: 1.0 },
        },
      ],
    };

    const passEncoder = commandEncoder.beginRenderPass(renderPassDescriptor);
    passEncoder.setPipeline(this.pipeline);
    passEncoder.draw(3, 1, 0, 0);
    passEncoder.endPass();
    this.gpu.defaultQueue.submit([commandEncoder.finish()]);
    requestAnimationFrame(this.update);
  }

  render() {
    return (
      <div style={{ margin: '5px' }}>
        <canvas width="500px" height="500px" ref={c => (this.canvasRef = c)} />
      </div>
    );
  }
}

export default TriangleComponent;

const glslShaders = {
  vertex: `#version 450
const vec2 pos[3] = vec2[3](vec2(0.0f, 0.5f), vec2(-0.5f, -0.5f), vec2(0.5f, -0.5f));

void main() {
    gl_Position = vec4(pos[gl_VertexIndex], 0.0, 1.0);
}
`,

  fragment: `#version 450
  layout(location = 0) out vec4 outColor;

  void main() {
      outColor = vec4(1.0, 0.0, 0.0, 1.0);
  }
`,
};
