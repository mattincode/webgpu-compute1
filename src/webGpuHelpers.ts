//Note: Any calculations using the GPU-device should not be performed on the UI-thread in a real world example!
export async function getDevice(): Promise<GPUDevice> {
  // https://gpuweb.github.io/gpuweb/#gpuadapter
  const adapter = await navigator.gpu.requestAdapter();
  const device = await adapter.requestDevice(); // Get GPU
  return device;
}
