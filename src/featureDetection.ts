export function hasWebGPUSupport() {
  return !!navigator.gpu;
}
