import React from 'react';
import './App.css';
import glslangModule, { GlslCompiler } from './glslang';
import { computeShader1 } from './shaders';
import { getDevice } from './webGpuHelpers';
import TriangleComponent from './TriangleComponent';

class GPUInfo {
  constructor(init?: Partial<GPUInfo>) {
    Object.assign(this, init);
  }
  name: string;
}

export interface IAppProps {
  name: string;
}

interface IAppState {
  gpuInfo: GPUInfo;
  output: string;
  compileStatus: string;
}

class App extends React.Component<IAppProps, IAppState> {
  constructor(props: IAppProps) {
    super(props);
    this.setupComputeBufferExample1 = this.setupComputeBufferExample1.bind(this);
    this.setupComputeBufferExample2 = this.setupComputeBufferExample2.bind(this);
    this.compileShader = this.compileShader.bind(this);
    this.setupComputeBufferExample3 = this.setupComputeBufferExample3.bind(this);
    this.state = { gpuInfo: new GPUInfo({ name: '' }), output: '', compileStatus: '' };
  }

  componentDidMount() {
    getDevice().then(gpu => {
      this.setState({ gpuInfo: { ...this.state.gpuInfo, name: gpu.adapter.name } }, () => {
        //this.setupComputeBufferExample1(gpu);
        //this.setupComputeBufferExample2(gpu);
        //this.compileShader();
        glslangModule().then(glslCompiler => {
          this.setupComputeBufferExample3(gpu, glslCompiler);
        });
      });
    });
  }

  // Example 1 - Get a GPU-buffer, write data in it from the CPU and the return it to the GPU
  setupComputeBufferExample1(gpu: GPUDevice): void {
    // Get a GPU buffer (CPU accessable)
    const buffer: GPUBuffer = gpu.createBuffer({
      usage: GPUBufferUsage.MAP_WRITE,
      size: 4,
      mappedAtCreation: true, // This is needed to be able to use raw access(getMappedRange) below (the buffer is mapped from the start)
    });
    // Get the raw buffer for writing
    const rawBuffer = buffer.getMappedRange();
    // Write bytes to buffer.
    new Uint8Array(rawBuffer).set([1, 2, 3, 4]);
    // Unmap buffer from CPU access and make it accessable to the GPU (prevents CPU/GPU race conditions)
    buffer.unmap();
  }

  // Example 2 - Copy buffer on the GPU using a GPU command
  setupComputeBufferExample2(gpu: GPUDevice): void {
    const data = [1, 2, 3, 4];
    // Get a GPU buffer in a mapped state
    const writeBuffer: GPUBuffer = gpu.createBuffer({
      usage: GPUBufferUsage.MAP_WRITE | GPUBufferUsage.COPY_SRC, // SRC-buffer
      size: data.length,
      mappedAtCreation: true,
    });
    // Get the raw buffer for writing
    const rawBuffer = writeBuffer.getMappedRange();
    // Write bytes to buffer.
    new Uint8Array(rawBuffer).set(data);
    // Unmap buffer from CPU access and make it accessable to the GPU (prevents CPU/GPU race conditions)
    writeBuffer.unmap();
    // Setup destination buffer
    const readBuffer = gpu.createBuffer({
      usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.MAP_READ,
      size: data.length,
    });
    // *** Copy buffer on the GPU ***
    const cmd = gpu.createCommandEncoder();
    const SRC_OFFSET = 0;
    const DST_OFFSET = 0;
    cmd.copyBufferToBuffer(writeBuffer, SRC_OFFSET, readBuffer, DST_OFFSET, data.length);
    gpu.defaultQueue.submit([cmd.finish()]);
    // *** Verify the copied data ***
    readBuffer.mapAsync(GPUMapMode.READ).then(() => {
      const rawReadBuffer = readBuffer.getMappedRange();
      this.setState({ output: new Uint8Array(rawReadBuffer).toString() });
    });
  }

  // Setup two buffers with matrix data and then a result buffer to store output from a matrix operation performed on the GPU using a compute shader.
  setupComputeBufferExample3(gpu: GPUDevice, glslCompiler: GlslCompiler): void {
    // *** Create matrix 1 ***
    // First 2 values in the matrix is the number of rows and columns!
    const ROW_COUNT = 256; //128;
    const COLUMN_COUNT = 512; // 256;
    const arrayLength = ROW_COUNT * COLUMN_COUNT;
    const matrix1Array = new Float32Array(arrayLength);
    matrix1Array[0] = ROW_COUNT;
    matrix1Array[1] = COLUMN_COUNT;
    for (let index = 2; index < arrayLength; index++) {
      matrix1Array[index] = index / 1000;
    }
    const gpuMatrix1Buffer = gpu.createBuffer({
      usage: GPUBufferUsage.STORAGE, // STORAGE because we need to access this from the compute shader
      size: matrix1Array.byteLength,
      mappedAtCreation: true,
    });
    const rawMatrix1Buffer = gpuMatrix1Buffer.getMappedRange();
    new Float32Array(rawMatrix1Buffer).set(matrix1Array);
    gpuMatrix1Buffer.unmap();
    // *** Create matrix 2 ***
    // Note: Doing a multiplication (hence flip rows/cols on the second matrix)
    const matrix2Array = new Float32Array(arrayLength);
    matrix2Array[0] = COLUMN_COUNT;
    matrix2Array[1] = ROW_COUNT;
    for (let index = 2; index < arrayLength; index++) {
      matrix2Array[index] = index / 1000;
    }
    const gpuMatrix2Buffer = gpu.createBuffer({
      usage: GPUBufferUsage.STORAGE, // STORAGE because we need to access this from the compute shader
      size: matrix2Array.byteLength,
      mappedAtCreation: true,
    });
    const rawMatrix2Buffer = gpuMatrix2Buffer.getMappedRange();
    new Float32Array(rawMatrix2Buffer).set(matrix2Array);
    gpuMatrix2Buffer.unmap();
    // *** Create result matrix ***
    const resultMatrixBufferSize = Float32Array.BYTES_PER_ELEMENT * (2 + matrix1Array[0] * matrix2Array[1]); // Note: 2 + <- add row/col-size!
    const gpuResultBuffer = gpu.createBuffer({
      usage: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_SRC,
      size: resultMatrixBufferSize,
    });
    // *** Setup shader input/output layout ***
    // Note: Below code is very likely to change in the future!
    const bindGroupLayout = gpu.createBindGroupLayout({
      entries: [
        {
          binding: 0,
          visibility: GPUShaderStage.COMPUTE,
          type: 'readonly-storage-buffer' as GPUBindingType,
        },
        {
          binding: 1,
          visibility: GPUShaderStage.COMPUTE,
          type: 'readonly-storage-buffer' as GPUBindingType,
        },
        {
          binding: 2,
          visibility: GPUShaderStage.COMPUTE,
          type: 'storage-buffer' as GPUBindingType,
        },
      ],
    });
    const bindGroup = gpu.createBindGroup({
      layout: bindGroupLayout,
      entries: [
        {
          binding: 0,
          resource: {
            buffer: gpuMatrix1Buffer,
          },
        },
        {
          binding: 1,
          resource: {
            buffer: gpuMatrix2Buffer,
          },
        },
        {
          binding: 2,
          resource: {
            buffer: gpuResultBuffer,
          },
        },
      ],
    });

    // *** Set up compute pipeline including shader and buffers
    const computePipeline = gpu.createComputePipeline({
      layout: gpu.createPipelineLayout({
        bindGroupLayouts: [bindGroupLayout],
      }),
      computeStage: {
        module: gpu.createShaderModule({
          code: glslCompiler.compileGLSL(computeShader1, 'compute'),
        }),
        entryPoint: 'main',
      },
    });
    const cmd = gpu.createCommandEncoder();
    const passEncoder = cmd.beginComputePass();
    passEncoder.setPipeline(computePipeline);
    const BINDGROUP_INDEX = 0;
    passEncoder.setBindGroup(BINDGROUP_INDEX, bindGroup);
    // Map x/y values to our matrix data
    passEncoder.dispatch(matrix1Array[0], matrix2Array[1]);
    passEncoder.endPass();
    // Setup buffer to get the result from the GPU (the result buffer setup earlier was for the GPU to write into)
    const gpuReadBuffer = gpu.createBuffer({
      usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.MAP_READ,
      size: resultMatrixBufferSize,
    });
    // Setup read command
    cmd.copyBufferToBuffer(
      gpuResultBuffer /* source buffer */,
      0 /* source offset */,
      gpuReadBuffer /* destination buffer */,
      0 /* destination offset */,
      resultMatrixBufferSize /* size */,
    );
    // *** Execute compute and read commands
    gpu.defaultQueue.submit([cmd.finish()]);
    // *** Read the result from the compute shader calculation
    gpuReadBuffer.mapAsync(GPUMapMode.READ).then(() => {
      const rawResultBuffer = gpuReadBuffer.getMappedRange();
      // Note: Result is in format: [rowCount, colCount, [0][0], [0][1], [1][0], [1][1]]
      this.setState({ output: new Float32Array(rawResultBuffer).toString(), compileStatus: 'OK' });
    });
  }

  // Test compilation of the compute shader
  compileShader() {
    this.setState({ compileStatus: 'Compile started...' });
    glslangModule().then(glslCompiler => {
      const compiledShader = glslCompiler.compileGLSL(computeShader1, 'compute');
      this.setState({ compileStatus: compiledShader.length > 0 ? 'OK' : 'FAIL' });
    });
  }

  render() {
    return (
      <div style={{ margin: '5px' }}>
        <h4>
          GPU adapter: <b>{this.state.gpuInfo.name}</b>
        </h4>
        <div>
          Output: <b>{this.state.output}</b>
        </div>
        <div>
          Shader compile: <b>{this.state.compileStatus}</b>
        </div>
        <TriangleComponent />
      </div>
    );
  }
}
/*
const App = () => <div>Hello React App from scratch</div>;
*/
export default App;
