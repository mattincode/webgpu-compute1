/* eslint-disable @typescript-eslint/ban-ts-comment */
let glslang = undefined;
export default async function (): Promise<GlslCompiler> {
  if (glslang !== undefined) return glslang;
  // @ts-ignore
  const glslangModule = await import(/* webpackIgnore: true */ 'https://unpkg.com/@webgpu/glslang@0.0.15/dist/web-devel/glslang.js');
  glslang = await glslangModule.default();
  return glslang;
}

export type ShaderType = 'compute' | 'vertex' | 'fragment';

export interface GlslCompiler {
  compileGLSL: (shaderCode: string, type: ShaderType) => string | Uint32Array;
}
